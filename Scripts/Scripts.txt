Use final_project;

CREATE TABLE rubric(
rubric_id INT AUTO_INCREMENT,
name_rubric VARCHAR(20),
PRIMARY KEY (rubric_id));

ALTER TABLE rubric AUTO_INCREMENT=1;



CREATE TABLE advert(
advert_id INT AUTO_INCREMENT,
name_advert VARCHAR(20),
date_advert DATE,
text_advert VARCHAR(100),
cost INT,
advert_author_fk INT NULL,
advert_rubric_fk INT NULL,
PRIMARY KEY (advert_id),
FOREIGN KEY(advert_author_fk) REFERENCES author(author_id),
FOREIGN KEY(advert_rubric_fk) REFERENCES rubric(rubric_id));

ALTER TABLE advert AUTO_INCREMENT=1;



CREATE TABLE author(
author_id INT AUTO_INCREMENT,
name_author VARCHAR(20),
author_phone_fk INT NULL,
author_address_fk INT NULL,
author_email_fk INT NULL,
PRIMARY KEY (author_id),
FOREIGN KEY(author_phone_fk) REFERENCES phone(phone_id),
FOREIGN KEY(author_address_fk) REFERENCES address(address_id),
FOREIGN KEY(author_email_fk) REFERENCES email(email_id));

ALTER TABLE author AUTO_INCREMENT=1;


CREATE TABLE phone(
phone_id INT AUTO_INCREMENT,
number_phone VARCHAR(20),
PRIMARY KEY (phone_id));

ALTER TABLE phone AUTO_INCREMENT=1;


CREATE TABLE email(
email_id INT AUTO_INCREMENT,
name_email VARCHAR(20),
PRIMARY KEY (email_id));

ALTER TABLE email AUTO_INCREMENT=1;


CREATE TABLE address(
address_id INT AUTO_INCREMENT,
city VARCHAR(20),
street VARCHAR(20),
number_home INT,
PRIMARY KEY (address_id));

ALTER TABLE address AUTO_INCREMENT=1;