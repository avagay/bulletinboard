package com.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Rubric.class)
public abstract class Rubric_ {

	public static volatile ListAttribute<Rubric, Advert> adverts;
	public static volatile SingularAttribute<Rubric, String> nameRubric;
	public static volatile SingularAttribute<Rubric, Integer> id;

}

