package com.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EMail.class)
public abstract class EMail_ {

	public static volatile SingularAttribute<EMail, Integer> id;
	public static volatile SingularAttribute<EMail, String> email;

}

