package com.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Advert.class)
public abstract class Advert_ {

	public static volatile SingularAttribute<Advert, LocalDate> date;
	public static volatile SingularAttribute<Advert, Integer> cost;
	public static volatile SingularAttribute<Advert, Author> author;
	public static volatile SingularAttribute<Advert, String> nameAdvert;
	public static volatile SingularAttribute<Advert, Rubric> rubric;
	public static volatile SingularAttribute<Advert, Integer> id;
	public static volatile SingularAttribute<Advert, String> text;

}

