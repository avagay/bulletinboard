package com.domain;

import javax.persistence.*;

@Entity
@SequenceGenerator(name = "seq_address_gen", sequenceName = "seq_address_gen", initialValue = 1, allocationSize = 1)
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_address_gen")
    private int id;

    private String city;

    private String street;

    @Column(name = "number_home")
    private int numberHome;

    public Address(String city, String street, int numberHome) {
        this.city = city;
        this.street = street;
        this.numberHome = numberHome;
    }

    public Address() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getNumberHome() {
        return numberHome;
    }

    public void setNumberHome(int numberHome) {
        this.numberHome = numberHome;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", numberHome=" + numberHome +
                '}';
    }
}
