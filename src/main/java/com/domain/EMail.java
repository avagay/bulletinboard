package com.domain;

import javax.persistence.*;

@Entity
@SequenceGenerator(name = "seq_email_gen", sequenceName = "seq_email_gen", initialValue = 1, allocationSize = 1)
public class EMail {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_email_gen")
    private int id;

    private String email;

    public EMail(String email) {
        this.email = email;
    }

    public EMail() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
