package com.domain;


import javax.persistence.*;

@Entity
@SequenceGenerator(name = "seq_phone_gen", sequenceName = "seq_phone_gen", initialValue = 1, allocationSize = 1)
public class Phone {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_phone_gen")
    private int id;

    private String number;

    public Phone(String number) {
        this.number = number;
    }

    public Phone() {}

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
