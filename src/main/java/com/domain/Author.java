package com.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator(name = "seq_author_gen", sequenceName = "seq_author_gen", initialValue = 1, allocationSize = 1)
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_author_gen")
    private int id;

    private String name;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "author_phone_fk")
    private Phone phone;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "author_address_fk")
    private Address address;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "author_email_fk")
    private EMail eMail;

    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REMOVE },
            orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "author")
    private List<Advert> adverts = new ArrayList<>();

    public Author(String name, Phone phone, Address address, EMail eMail) {
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.eMail = eMail;
    }

    public Author() {}

    public void addAdvert(Advert advert) {
        adverts.add(advert);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public EMail geteMail() {
        return eMail;
    }

    public void seteMail(EMail eMail) {
        this.eMail = eMail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone=" + phone.getNumber() +
                ", address=" + address +
                ", eMail=" + eMail.getEmail() +
                '}';
    }
}
