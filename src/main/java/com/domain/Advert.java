package com.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.serializator.AdvertDeserializator;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@SequenceGenerator(name = "seq_advert_gen", sequenceName = "seq_advert_gen", initialValue = 1, allocationSize = 1)
//@JsonDeserialize(using = AdvertDeserializator.class)
public class Advert {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_advert_gen")
    private int id;

    @Column(name = "name_advert")
    private String nameAdvert;

    private LocalDate date;

    private String text;

    private int cost;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "advert_author_fk")
    private Author author;

    @ManyToOne
    @JoinColumn(name = "rubric_advert_fk")
    private Rubric rubric;

    public Advert(String nameAdvert, LocalDate date, String text, int cost, Author author, Rubric rubric) {
        this.nameAdvert = nameAdvert;
        this.date = date;
        this.text = text;
        this.cost = cost;
        this.author = author;
        this.rubric = rubric;
    }

    public Advert(String nameAdvert, LocalDate date, String text, int cost) {
        this.nameAdvert = nameAdvert;
        this.date = date;
        this.text = text;
        this.cost = cost;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameAdvert() {
        return nameAdvert;
    }

    public void setNameAdvert(String nameAdvert) {
        this.nameAdvert = nameAdvert;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Rubric getRubric() {
        return rubric;
    }

    public void setRubric(Rubric rubric) {
        this.rubric = rubric;
    }

    public Advert() {

    }

    @Override
    public String toString() {
        return "Advert{" +
                "id=" + id +
                ", nameAdvert='" + nameAdvert + '\'' +
                ", date=" + date +
                ", text='" + text + '\'' +
                ", cost=" + cost +
                ", author=" + author +
                ", rubric=" + rubric.getNameRubric() +
                '}';
    }
}


