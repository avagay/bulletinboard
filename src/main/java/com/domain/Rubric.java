package com.domain;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator(name = "seq_rubric_gen", sequenceName = "seq_rubric_gen", initialValue = 1, allocationSize = 1)
public class Rubric {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_rubric_gen")
    private int id;

    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REMOVE },
            orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "rubric")
    private List<Advert> adverts = new ArrayList<>();

    @Column(name = "name_rubric")
    private String nameRubric;

    public Rubric(String nameRubric) {
        this.nameRubric = nameRubric;
    }

    public Rubric(){

    }

    public void addAdvert(Advert advert) {
        adverts.add(advert);
    }

    public List<Advert> getAdverts() {
        return adverts;
    }

    public void setAdverts(List<Advert> adverts) {
        this.adverts = adverts;
    }

    public String getNameRubric() {
        return nameRubric;
    }

    public void setNameRubric(String nameRubric) {
        this.nameRubric = nameRubric;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
