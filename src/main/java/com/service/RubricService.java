package com.service;

import com.domain.Advert;
import com.domain.Rubric;
import com.domain.Rubric_;
import com.repository.RubricRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.util.List;

@Service
@Transactional
public class RubricService  {

    @Autowired
    private RubricRepository rubricRepository;

    public void save(Rubric rubric) {

        rubricRepository.save(rubric);
    }


    /*Удалять рубрику*/
    public void remoteRubric(int id) {

        rubricRepository.deleteRubricById(id);
    }


    /*Показывать объявления из одной рубрики и нескольких рубрик*/
    public void showAdvertFromRubric(int id) {

        List<Advert> adverts = rubricRepository.getAdvertsById(id);

        System.out.println(adverts.get(0));


        /*CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Tuple> tupleQuery = cb.createTupleQuery();

        Root<Rubric> root = tupleQuery.from(Rubric.class);

        Path<String> pathNameRubric = root.get(Rubric_.nameRubric);

        tupleQuery.where(cb.equal(pathNameRubric, nameRubric));

        Expression<List<Advert>> listExpression = root.get(Rubric_.adverts);

        tupleQuery.multiselect(listExpression);

        tupleQuery.where(cb.equal(pathNameRubric, nameRubric));

        TypedQuery<Tuple> query = em.createQuery(tupleQuery);

        List<Tuple> resultList = query.getResultList();

        System.out.println(resultList.get(0).get(listExpression));
*/


    }



}
