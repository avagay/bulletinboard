package com.service;

import com.domain.Advert;
import com.repository.AdvertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;

@Transactional
@Service
public class AdvertService {

    @Autowired
    private AdvertRepository advertRepository;

    public void save(Advert advert) {
        advertRepository.save(advert);

    }

}
