package com.service;

import com.domain.*;
import com.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

@Service
@Transactional
public class AuthorService {

    @Autowired
    private AuthorRepository authorRepository;


    public void save(Author author) {
        authorRepository.save(author);
    }

    /*Удалять все объявления автора.*/
    public void remoteAuthor(int id) {

        authorRepository.deleteAuthorById(id);

    }
}
