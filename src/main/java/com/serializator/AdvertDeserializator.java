package com.serializator;

import com.domain.Advert;
import com.domain.Author;
import com.domain.Rubric;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Objects;

public class AdvertDeserializator extends StdDeserializer<Advert> {

    public AdvertDeserializator(Class<?> t) {
        super(t);
    }

    public AdvertDeserializator() {
        this(null);
    }

    @Override
    public Advert deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {

        Advert advert = new Advert();

        ObjectCodec codec = p.getCodec();

        JsonNode node = codec.readTree(p);

        JsonNode treeNode = node.get("date");

        if (Objects.nonNull(treeNode)) {
            LocalDate date = LocalDate.parse(treeNode.asText());

            advert.setDate(date);
        }

        JsonNode id = node.get("id");

        if (Objects.nonNull(id)) {
            advert.setId(id.asInt());
        }

        advert.setNameAdvert(node.get("nameAdvert").asText());
        advert.setText(node.get("text").asText());

        advert.setCost(node.get("cost").asInt());

        JsonNode authorNode = node.get("author");

        Author author = new Author();

        author.setId(authorNode.get("id").asInt());

        advert.setAuthor(author);

        JsonNode rubricNode = node.get("rubric");

        Rubric rubric = new Rubric();

        rubric.setId(rubricNode.get("id").asInt());

        advert.setRubric(rubric);

        return advert;
    }
}
