package com.repository;

import com.domain.Advert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Transactional
public interface AdvertRepository extends JpaRepository<Advert, Integer> {
     Advert findByTextAndDate(String text, LocalDate localDate);

     @Query("SELECT a FROM Advert a INNER JOIN a.author INNER JOIN a.rubric WHERE a.text =:a_text AND a.cost =:a_cost")
     Advert findByTextAndCost(@Param("a_text") String text, @Param("a_cost") int cost);

     @Modifying
     @Query("UPDATE Advert a SET a.text =:a_text")
     int updateAdvertByText(@Param("a_text") String text);






}
