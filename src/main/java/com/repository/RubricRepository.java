package com.repository;

import com.domain.Advert;
import com.domain.Rubric;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Transactional
public interface RubricRepository extends JpaRepository<Rubric, Integer> {

    void deleteRubricById(int id);

    List<Advert> getAdvertsById(int id);



}
