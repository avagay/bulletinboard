package com.repository;

import com.domain.Advert;
import com.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Transactional
public interface AuthorRepository extends JpaRepository<Author, Integer> {

    void deleteAuthorById(int id);




}
