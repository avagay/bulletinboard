package com.controller;

import com.domain.Author;
import com.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/author")
public class AuthorController {

    @Autowired
    private AuthorService service;

    @PostMapping(value = "/save")
    public void save(@RequestBody Author author) {
        service.save(author);
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteAutorById(@PathVariable("id") int id) {
        service.remoteAuthor(id);
    }
}
