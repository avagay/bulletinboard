package com.controller;

import com.domain.Advert;
import com.service.AdvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/advert")
public class AdvertController {

    @Autowired
    private AdvertService service;

    @PostMapping(value = "/save")
    public void save(@RequestBody Advert advert) {
        service.save(advert);

        System.out.println();
    }
}
