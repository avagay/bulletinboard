package com.controller;

import com.domain.Rubric;
import com.service.RubricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rubric")
public class RubricController {

    @Autowired
    private RubricService service;

    @PostMapping(value = "/save")
    public void save(@RequestBody Rubric rubric) {
        service.save(rubric);
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteRubricById(@PathVariable("id") int id){

        service.remoteRubric(id);
    }

    @GetMapping(value = "/adverts/{id}")
    public void getAdvertsById(@PathVariable("id") int id){

        service.showAdvertFromRubric(id);
    }

}
